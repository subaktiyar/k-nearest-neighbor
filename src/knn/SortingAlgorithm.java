/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knn;

/**
 *
 * @author Subaktiyar
 */
public class SortingAlgorithm {

    /**
     * 
     */
    public SortingAlgorithm() {
    
    }
    
    /**
     * 
     * @param arrayData 
     * @param arrayClass 
     */
    public static final void bubbleSort(double[][] arrayData, int[] arrayClass) {
        double[] temp;
        int tempClass;
        for (int i = 0; i < arrayData.length; i++) {
            for (int j = i + 1; j < arrayData.length; j++) {
                if (arrayData[i][0] > arrayData[j][0]) {
                    temp = arrayData[i];
                    arrayData[i] = arrayData[j];
                    arrayData[j] = temp;
                    
                    tempClass = arrayClass[i];
                    arrayClass[i] = arrayClass[j];
                    arrayClass[j] = tempClass;
                }
            }
        }
    }
    
    /**
     * 
     * @param arrayData 
     * @param arrayClass 
     */
    public static final void bubbleSort(double[] arrayData, int[] arrayClass) {
        double tempData;
        int tempClass;
        for (int i = 0; i < arrayData.length; i++) {
            for (int j = i + 1; j < arrayData.length; j++) {
                if (arrayData[i] > arrayData[j]) {
                    tempData = arrayData[i];
                    arrayData[i] = arrayData[j];
                    arrayData[j] = tempData;
                    
                    tempClass = arrayClass[i];
                    arrayClass[i] = arrayClass[j];
                    arrayClass[j] = tempClass;
                }
            }
        }
    }
    
    public static final void bubbleSort(int [] arrayData) {
        int temp;
        for (int i = 0; i < arrayData.length; i++) {
            for (int j = i + 1; j < arrayData.length; j++) {
                if (arrayData[i] > arrayData[j]) {
                    temp = arrayData[i];
                    arrayData[i] = arrayData[j];
                    arrayData[j] = temp;
                }
            }
        }
    }
    
}
