/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knn;

/**
 *
 * @author Subaktiyar
 */
public class Distance {

    public Distance() {
    
    }
    
    /**
     * Function Calculation of Chebyshev
     *
     * @param _mData
     * @param _mUji
     * @return
     */
    public static final double getChebyshevDistance(int[] _mData, int[] _mUji) {
        double value = 0.0;
        for (int i = 0; i < _mData.length; i++) {
//            System.out.println((_mUji[i] - _mData[i]));
            value = Math.max(value, Math.abs(_mUji[i] - _mData[i]));
        }
        return value;
    }
    
    /**
     * Function Calculation of Manhattan
     *
     * @param _mData
     * @param _mUji
     * @return
     */
    public static final double getManhattanDistance(int[] _mData, int[] _mUji) {
        double value = 0.0;
        for (int i = 0; i < _mData.length; i++) {
//            System.out.println((_mUji[i] - _mData[i]));
            value += Math.abs(_mUji[i] - _mData[i]);
        }
        return value;
    }
    
    /**
     * Function Calculation of Euclidean
     *
     * @param _mData
     * @param _mUji
     * @return
     */
    public static final double getEuclideanValue(int[] _mData, int[] _mUji) {
        double value = 0.0;
        for (int i = 0; i < _mData.length; i++) {
//            System.out.println((_mUji[i] - _mData[i]));
            value += Math.pow((_mUji[i] - _mData[i]), 2);
        }
        return Math.sqrt(value);
    }
}
