/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knn;

import java.text.DecimalFormat;

/**
 *
 * @author Subaktiyar
 */
public class KNearestNeighbor {

    private int mData[][];

    private int mTarget[];

    private int mUji[];

    private double mHasil[];

    private int mHasilSorting[];

    private static final int MAX_DATA = 17;
    
    private static final int MAX_CLASS = 3;
    
    private int[] countClass;

    public KNearestNeighbor() {
        this.mData = new int[][]{
            {1, 1},
            {2, 1},
            {3, 1},
            {3, 2},
            {7, 2},
            {1, 3},
            {2, 3},
            {5, 3},
            {4, 4},
            {6, 4},
            {1, 5},
            {6, 5},
            {1, 6},
            {4, 6},
            {5, 6},
            {2, 7},
            {4, 7}
        };
        this.mTarget = new int[]{
            3, 3, 1, 3, 2, 3, 1, 2, 3, 2, 3, 2, 1, 2, 3, 2, 2
        };
        this.mUji = new int[]{
            3, 4
        };
        this.mHasil = new double[MAX_DATA];
        this.mHasilSorting = new int[MAX_DATA];
        this.countClass = new int[MAX_CLASS];
        for (int i = 0; i < countClass.length; i++) {
            countClass[i] = 0;
        }
    }

    public static void main(String[] args) {
        KNearestNeighbor knn = new KNearestNeighbor();

        System.out.println("\n--> Data Matriks");
        knn.printMatrix(knn.getmData());

        System.out.println("\n--> Target Data");
        knn.printMatrix(knn.getmTarget());

        System.out.println("\n\n--> Data Uji");
        knn.printMatrix(knn.getmUji());

        System.out.println("\n\n--> Hasil Classification");
        knn.procClassification();
        knn.printMatrix(knn.getmHasil());
        
        System.out.println("\n\n--> Hasil Sorting Classification");
        SortingAlgorithm.bubbleSort(knn.getmHasil(), knn.getmTarget());
        knn.printMatrix(knn.getmHasil());
        
        System.out.println("\n--> Target Data Baru");
        knn.printMatrix(knn.getmTarget());
        
        int k = 5;
        System.out.println("\n\n--> Data Uji Termasuk Kelas (k = " + k + ") : " + knn.getClassification(knn.getmTarget(), k));
        
        System.out.println("");
    }

    /**
     * 
     * @param _data
     * @param k
     * @return 
     */
    public int getClassification(int [] _data, int k) {
        for (int index = 0; index < k; index++) {
            for (int i = 1; i <= MAX_CLASS; i++) {
                if (_data[index] == i) {
                    getCountClass()[i - 1] = getCountClass()[i - 1] + 1;
                    break;
                }
            }
        }
        int indexClass = 0;
        int temp = getCountClass()[0];
        for (int index = 1; index < MAX_CLASS; index++) {
            if (getCountClass()[index] > temp) {
                temp = getCountClass()[index];
                indexClass = index;
            }
        }
        return indexClass + 1;
    }
    
    /**
     * 
     */
    public void procClassification() {
        for (int index = 0; index < MAX_DATA; index++) {
//            getmHasil()[index] = Distance.getEuclideanValue(getmData()[index], getmUji());
            getmHasil()[index] = Distance.getManhattanDistance(getmData()[index], getmUji());
//            getmHasil()[index] = Distance.getChebyshevDistance(getmData()[index], getmUji());
        }
    }

    /**
     * Print Matrix 1 Dimension
     *
     * @param data
     */
    public void printMatrix(int[] data) {
        for (int d : data) {
            String val = new DecimalFormat("#0.0").format(d);
            System.out.print(val + " ");
        }
    }

    /**
     * Print Matrix 1 Dimension
     *
     * @param data
     */
    public void printMatrix(double[] data) {
        for (double d : data) {
            String val = new DecimalFormat("#0.0").format(d);
            System.out.print(val + " ");
        }
    }

    /**
     * Print Matrix 2 Dimension
     *
     * @param data
     */
    public void printMatrix(int[][] data) {
        for (int[] d : data) {
            printMatrix(d);
            System.out.println();
        }
    }

    /**
     * Print Matrix 2 Dimension
     *
     * @param data
     */
    public void printMatrix(double[][] data) {
        for (double[] d : data) {
            printMatrix(d);
            System.out.println();
        }
    }

    /**
     * @return the mData
     */
    public int[][] getmData() {
        return mData;
    }

    /**
     * @param mData the mData to set
     */
    public void setmData(int[][] mData) {
        this.mData = mData;
    }

    /**
     * @return the mTarget
     */
    public int[] getmTarget() {
        return mTarget;
    }

    /**
     * @param mTarget the mTarget to set
     */
    public void setmTarget(int[] mTarget) {
        this.mTarget = mTarget;
    }

    /**
     * @return the mHasil
     */
    public double[] getmHasil() {
        return mHasil;
    }

    /**
     * @param mHasil the mHasil to set
     */
    public void setmHasil(double[] mHasil) {
        this.mHasil = mHasil;
    }

    /**
     * @return the mHasilSorting
     */
    public int[] getmHasilSorting() {
        return mHasilSorting;
    }

    /**
     * @param mHasilSorting the mHasilSorting to set
     */
    public void setmHasilSorting(int[] mHasilSorting) {
        this.mHasilSorting = mHasilSorting;
    }

    /**
     * @return the mUji
     */
    public int[] getmUji() {
        return mUji;
    }

    /**
     * @param mUji the mUji to set
     */
    public void setmUji(int[] mUji) {
        this.mUji = mUji;
    }

    /**
     * @return the countClass
     */
    public int[] getCountClass() {
        return countClass;
    }

    /**
     * @param countClass the countClass to set
     */
    public void setCountClass(int[] countClass) {
        this.countClass = countClass;
    }

}
